import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.options
import os.path
import json
from tornado.options import define, options
import socketio

sio = socketio.AsyncServer(async_mode='tornado')

define("port", default=80, help="run on the given port", type=int)

class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

@sio.on('GetIP')
async def GetIP(sid, ip):
    with open("Users.json") as Data:
        Data = json.load(Data)
        UsersIP = Data["IP"]
        Users = Data["Users"]
    print(ip)
    if ip[0] in UsersIP:
        await sio.emit("User?",[True,Users[ip[0]]])
        return
    elif ip[1]:
        AddUsers(ip[1],ip[0])
        await sio.emit("User?",[True,ip[1]])
    await sio.emit("User?",[False])

@sio.on('sent')
async def sent(sid, msg):
    with open("History.json",'r+') as f:
        Data = json.load(f)
        Data.append(msg)
        f.seek(0)
        json.dump(Data, f, indent=4)
        f.truncate()
    await sio.emit("received",msg)

@sio.on('Connect')
def Connect(sid,msg):
    print(msg)
    with open("ip.json",'r+') as f:
        Data = json.load(f)
        if not msg in Data:
            Data.append(msg)
        f.seek(0)
        json.dump(Data, f, indent=4)
        f.truncate()

def AddUsers(Name, IP):
    with open("Users.json",'r+') as f:
        Data = json.load(f)
        if not IP in Data['IP']:
            Data['IP'].append(IP)
        Data['Users'][IP] = Name
        try:
            if not IP in Data['Names'][Name]:
                Data['Names'][Name].append(IP)
        except:
            Data['Names'][Name] = [IP]
        f.seek(0)
        json.dump(Data, f, indent=4)
        f.truncate()

@sio.on('Name')
async def Name(sid, User):
    AddUsers(User[1],User[0])


class Chat(BaseHandler):
    async def get(self):
        with open("History.json") as f:
            Data=json.load(f)
        self.render('Chat.html')
        @sio.on("OK")
        async def Run(sid,User):
            await sio.emit("Startup",[self.get_cookie("Name"),Data])

class Main(BaseHandler):
    def get(self):
        self.render('AboutBen.html')

class Application(tornado.web.Application):
    def __init__(self):
        base_dir = os.path.dirname(__file__)
        settings = {
            "cookie_secret": "VEdsbVpXbHpaMjl2WkhOdmFYTnVhV2RuWlhKemEyVmxjSGRvYVhSbGNHOTNaWEpvYVdkbw==",
            'template_path': os.path.join(base_dir, "views"),
            'static_path': os.path.join(base_dir, "static"),
            'debug':True,
            "xsrf_cookies": False,
        }

        tornado.web.Application.__init__(self, [
            tornado.web.url(r"/", Main, name="Main"),
            tornado.web.url(r"/Chat", Chat, name="Main"),
            (r"/socket.io/", socketio.get_tornado_handler(sio))
        ], **settings)

def main():
    tornado.options.parse_command_line()
    Application().listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

print("Startup is starting on url: http://localhost:"+str(options.port))
main()
