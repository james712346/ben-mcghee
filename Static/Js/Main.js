var socket = io.connect('http://' + document.domain + ':' + location.port);
var ip;
var name = "New";
var date;
var Cookie;
var Start = true;
days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

$.getJSON('http://gd.geobytes.com/GetCityDetails?callback=?', function(data) {
  console.log(JSON.stringify(data, null, 2));
  ip = data["geobytesremoteip"];
  socket.emit("Connect",ip)
});
console.log(ip)
function AddMsg(msg){
  var message = $('.message').first().clone();
  message.find('.msg').text(msg[0]);
  message.find('.user').text(msg[1]);
  message.find('.datetime').text(msg[2]);
  message.prependTo('.chat-container');
};

$(function () {
    //If user Enter a Message
    $('#EnterMsg').submit(function(){
      date = new Date();
      date = days[date.getDay()-1] + " " + date.getDate() + '/' + date.getMonth() + "/" + date.getYear() + " " + date.getHours()+ ":" + date.getMinutes();
      msg = [$('#Smsg').val(),name,date,ip];
      console.log(msg);
      socket.emit('sent', msg);
      $('#Smsg').val('');
      return false;
    });

    socket.on('Startup', function(msg){
      console.log(msg);
      if (Start){
        Cookie = msg[0];
        $.getJSON('http://gd.geobytes.com/GetCityDetails?callback=?', function(data) {
          ip = data["geobytesremoteip"];
          socket.emit("GetIP",[ip,Cookie]);
        });

        for (message in msg[1]){
          AddMsg(msg[1][message]);
          console.log(message);
        };
        Start = false;
      };
    });
    //If the server send a Message
    socket.on('received', function(msg){
      AddMsg(msg);
    });

    //Check if user is already in DataBase
    socket.on('User?', function(msg){
      console.log(msg);
      if (msg[0]){
        $('#EnterName').hide();
        $('#EnterMsg').show();
        $('#Username').show();
        name = msg[1];
        document.cookie = "Name="+msg[1];
        $('#Username').text("Welcome " +name);
        return false;
      }else{
        $('#EnterMsg').hide();
        $('#Username').hide();
      };
    });

    //Send New User to Database
    $('#EnterName').submit(function(){
      socket.emit("Name",[ip,$('#name').val()]);
      document.cookie = "Name="+$('#name').val();
      name = $('#name').val();
      $('#EnterName').hide();
      $('#EnterMsg').show();
      $('#Username').show();
      $('#Username').text("Welcome " + name);
      return false;
    });

  });
async function go(){
  console.log("Start");
  await socket.emit("OK","lol");
};
go();
